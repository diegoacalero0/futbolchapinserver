const fetch = require("node-fetch");
const admin = require('./firebaseAdmin');

let matchs = [];

function getMatchInfo(matchId){
    return new Promise( (resolve, reject) => {
        fetch('http://futbolchapinenvivo.com/html/v3/htmlCenter/data/deportes/futbol/guatemala/events/' + matchId + '.json', {
            
        })
        .then((response) => response.json())
        .then((response) => {
            resolve(response);
        })
        .catch((error) => {
            reject(error);
        })
    });
}

setInterval(() => {
    fetch('http://futbolchapinenvivo.com/html/v3/htmlCenter/data/deportes/futbol/guatemala/agendaMaM/es/agenda.json')
    .then((response) => response.json())
    .then((response) => {
        response = response.events;
        let arr = [];

        let promisesArr = [];

        let curr = new Date();
        let first =  curr.getDate() - curr.getDay() + (curr.getDay() === 0 ? -6 : 1);
        let firstday = new Date(curr.setDate(first));
        let lastday = new Date(curr.setDate(firstday.getDate()+6));

        for(let prop in response){
            response[prop].matchId = prop.split('.')[3];

            let dateString  = response[prop].date.toString();
            let year        = dateString.substring(0,4);
            let month       = dateString.substring(4,6);
            let day         = dateString.substring(6,8);
            let date        = new Date(year, month-1, day, 0, 0, 0);
            if(date >= firstday && date <= lastday){
                arr.push(response[prop]);
                promisesArr.push(getMatchInfo(response[prop].matchId));
            }
        }

        Promise.all(promisesArr)
        .then((values) => {
            for(let i = 0; i < values.length; i ++){
                arr[i].venueInformation = values[i].venueInformation;
                arr[i].scoreStatus = values[i].scoreStatus;
            }

            for(let i = 0; i < arr.length; i++){
                for(let j = 0; j < matchs.length; j++){
                    let current = arr[i];
                    let before = matchs[j];
                    if(current.matchId == before.matchId){
                        if(current.statusId != before.statusId){
                            if(current.statusId == 1){
                                admin.sendStartMatchNotification(current);
                            }else if(current.statusId == 2){
                                admin.sendFinishMatchNotification(current);
                            }
                        }

                        let scoreHome = current.scoreStatus[current.teams.homeTeamId].score;
                        let scoreAway = current.scoreStatus[current.teams.awayTeamId].score;

                        let scoreHomeBefore = before.scoreStatus[before.teams.homeTeamId].score;
                        let scoreAwayBefore = before.scoreStatus[before.teams.awayTeamId].score;
                        
                        if(scoreHome != null && scoreAway != null){
                            if(scoreHome != 0 || scoreAway != 0){
                                if(scoreHome != scoreHomeBefore){
                                    admin.sendGoalNotification(current, 0)
                                }

                                if(scoreAway != scoreAwayBefore){
                                    admin.sendGoalNotification(current, 1)
                                }

                            }
                        }
                    }

                    

                }
            }

            matchs = arr;
            
        })

        
    }).catch((error) => {
        alert("Ha ocurrido un error al obtener los eventos: ", error);
    });
}, 20000)