const express = require('express');
const formidable = require('express-formidable');
const app = express();
const admin = require('./firebaseAdmin');


app.use(formidable({keepExtensions: true}));

app.get('/', (req, res) => {
  res.send('This is the main url of the server')
})

app.post('/registerToken', (req, res) => {
  console.log(req.fields);
  console.log('hola');
  let token = req.fields.token;
  admin.subscribeToTopic(token, res);
});

app.get('/sendTestNotification', (req, res) => {
  admin.sendTestNotification(res);
});


module.exports = app;