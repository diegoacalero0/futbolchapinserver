var admin = require('firebase-admin');
var serviceAccount = require('./futbolchapinapp.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://futbolchapinapp.firebaseio.com'
});

var test = {
    notification: {
      title: 'Volvieron las notificaciones!',
      body: 'Desde ahora recibirás notificaciones!'
    },
    topic: 'futbolchapin',
    android: {
        notification: {
            sound: 'default'
        },
        priority: 'high'
    }
};

module.exports = {
    sendTestNotification: (res) => { 
        admin.messaging().send(test)
        .then((response) => {
           res.send(response);
        })
        .catch((error) => {
            res.send(error);
        });
    },
      
    sendStartMatchNotification: (match) => {
        var message = {
            notification: {
              title: 'Acaba de iniciar el partido',
              body: match.teams.homeTeamName + ' VS ' + match.teams.awayTeamName
            },
            topic: 'futbolchapin',
            android: {
                notification: {
                    sound: 'default'
                },
                priority: 'high'
            }
        };

        admin.messaging().send(message)
        .then((response) => {
            console.log(response);
        })
        .catch((error) => {
            console.log(error);
        });
    },
    
    sendFinishMatchNotification: (match) => {
        var message = {
            notification: {
              title: 'Partido finalizado',
              body: match.teams.homeTeamName + ' ' + match.scoreStatus[match.teams.homeTeamId].score + ' - ' + match.scoreStatus[match.teams.awayTeamId].score + ' ' + match.teams.awayTeamName
            },
            topic: 'futbolchapin',
            android: {
                notification: {
                    sound: 'default'
                },
                priority: 'high'
            }
        };

        admin.messaging().send(message)
        .then((response) => {
            console.log(response);
        })
        .catch((error) => {
            console.log(error);
        });

    },
    
    sendGoalNotification: (match, team) => {
        var message = {
            notification: {
              title: 'Gooooool! de ' + (team == 0 ? match.teams.homeTeamName : match.teams.awayTeamName),
              body: match.teams.homeTeamName + ' ' + match.scoreStatus[match.teams.homeTeamId].score + ' - ' + match.scoreStatus[match.teams.awayTeamId].score + ' ' + match.teams.awayTeamName
            },
            topic: 'futbolchapin',
            android: {
                notification: {
                    sound: 'default'
                },
                priority: 'high'
            }
        };

        admin.messaging().send(message)
        .then((response) => {
            console.log(response);
        })
        .catch((error) => {
            console.log(error);
        });
    },
    
    subscribeToTopic: (token, res) => {
        var tokens = [token];
        admin.messaging().subscribeToTopic(tokens, 'futbolchapin')
        .then(function(response) {
            console.log(response + " Token: " + token + " registrado correctamente");
            res.send(response);
        })
        .catch(function(error) {
            console.log(error + " Error al registrar el Token: " + token);
            res.send(error);
        });
    }
}

