const app = require('./index');
const server = require('http').Server(app);
const updater = require('./updater');

server.listen(8001, () => {
    console.log('server on port', 8001);
});